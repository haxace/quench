package com.example.harshitsaxena.quench;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by harshitsaxena on 3/1/16.
 */
public class ChartView extends View {
    Paint paint;
    public ChartView(Context context) {
        super(context);
    }

    public ChartView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ChartView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    protected void onDraw(Canvas canvas) {
        Paint p2=new Paint();
        Paint p3=new Paint();
        Paint p1=new Paint();
        p1.setAntiAlias(true);
        p1.setColor(Color.parseColor("#9e9e9e"));
        p2.setAntiAlias(true);
        p2.setColor(Color.parseColor("#009688"));
        p3.setAntiAlias(true);
        p3.setColor(Color.parseColor("#F44336"));
        paint=new Paint();
        paint.setColor(Color.BLACK);
        paint.setStrokeWidth(1f);
        int x=getWidth();
        int y=getHeight();
        canvas.drawLine(0,y/2 , x, y/2, p1);
        canvas.drawLine(x/4, 5*y/8, x/4, 3*y/8, p1);
        canvas.drawLine(x/2, 5*y/8, x/2, 3*y/8, p1);
        canvas.drawLine(3*x/4, 5*y/8, 3*x/4, 3*y/8, p1);
        RectF rectF1 = new RectF(x/8-25, y/2, x/8+25, y/2+60);
        canvas.drawRect(rectF1,p3);
        RectF rectF2 = new RectF(3*x/8-25, 40, 3*x/8+25, y/2);
        canvas.drawRect(rectF2,p2);
        RectF rectF3 = new RectF(5*x/8-25, y/2, 5*x/8+25, y-10);
        RectF rectF4 = new RectF(7*x/8-25, y/2-60, 7*x/8+25, y/2);
        canvas.drawRect(rectF3,p3);
        canvas.drawRect(rectF4,p2);




    }

}
