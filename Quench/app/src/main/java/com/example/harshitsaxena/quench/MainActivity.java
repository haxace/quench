package com.example.harshitsaxena.quench;

import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.nfc.tech.MifareClassic;
import android.nfc.tech.NfcA;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import android.nfc.Tag;
import android.nfc.tech.MifareUltralight;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity implements NfcAdapter.CreateNdefMessageCallback{
    TextView mTag, helperText, credits;
    boolean cardFound, success;
    MyDatabaseAdapter myDatabaseAdapter;
    EditText credit;
    int quantity;
    FloatingActionButton add;
    LinearLayout card, cardActive;
    NfcAdapter mNfcAdapter;
    ArrayList<String> list;
    PendingIntent pendingIntent;
    private SimpleCursorAdapter simpleCursorAdapter;
    private ArrayAdapter arrayAdapter;
    int currentCredit = 0;
    int cardId;
    MifareClassic mifare;
    IntentFilter[] intentFiltersArray;
    Cursor cursor;
    String[][] techListsArray;
    Tag tagFromIntent;
    Button loadCredit;
    ProgressBar loader;

    byte[] c={0x01,0x01,0x01,0x01,0x01,0x01};
    @Override
    protected void onResume() {
        super.onResume();
        mNfcAdapter.enableForegroundDispatch(this, pendingIntent, null, null);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mNfcAdapter.disableForegroundDispatch(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())) {
            Parcelable[] rawMessages = intent.getParcelableArrayExtra(
                    NfcAdapter.EXTRA_NDEF_MESSAGES);

            NdefMessage message = (NdefMessage) rawMessages[0];
            // only one message transferred
            String value=new String(message.getRecords()[0].getPayload());
            Integer values=Integer.parseInt(value);
            Log.e("text",values.toString());
            SharedPreferences sharedPreferences = this.getSharedPreferences("Stats", 0);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt("Credits", sharedPreferences.getInt("Credits", 0) + values);

            editor.putString("Recharge", now());
            editor.commit();

        }
        else{
        tagFromIntent = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        credits.setText(readTag(tagFromIntent,1));
        toggleView();}
    }

    public int toInt(byte[] payload, int n) {
        byte[] a = new byte[n];
        System.arraycopy(payload, 0, a, 0, n);
        int answer = ByteBuffer.wrap(a).getInt();
        return answer;
    }
    public static String now() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("MMMM d, yyyy");
        return sdf.format(cal.getTime());
    }
    public void toggleView() {
        if (cardFound) {
            card.setVisibility(View.INVISIBLE);
            cardActive.setVisibility(View.VISIBLE);
        } else {
            cardActive.setVisibility(View.INVISIBLE);
            card.setVisibility(View.VISIBLE);
        }
    }

    public String readTag(Tag tag,int sector) {
         mifare = MifareClassic.get(tag);
//        Log.d("test", "read "+mifare.toString());
        try {
            mifare.connect();
            if(sector==0){
                boolean validate=mifare.authenticateSectorWithKeyA(0,MifareClassic.KEY_DEFAULT);

                //byte[] payload = mifare.readBlock(0);
                if(validate){
                    byte[] cardInfo = mifare.readBlock(0);
                    //byte[] cardCredit = mifare.readBlock(4);
                    System.out.println(Arrays.toString(cardInfo));
                    //Toast.makeText(getApplicationContext(), "Card Read", Toast.LENGTH_SHORT).show();
                    //currentCredit = toInt(cardCredit, 16);
                    cardId = toInt(cardInfo, 16);
                    cardFound = true;
                    return String.valueOf(cardId);}

            }
            else{
                boolean validate=mifare.authenticateSectorWithKeyA(1,c);

                //byte[] payload = mifare.readBlock(0);
                if(validate){
                    //byte[] cardInfo = mifare.readBlock(4);
                    byte[] cardCredit = mifare.readBlock(4);
                    //System.out.println(Arrays.toString(cardInfo));
                    //Toast.makeText(getApplicationContext(), "Card Read", Toast.LENGTH_SHORT).show();
                    currentCredit = toInt(cardCredit, 16);
                    //cardId = toInt(cardInfo, 16);
                    cardFound = true;
                    return String.valueOf(currentCredit);}

            }

        } catch (IOException e) {
            Log.e("test", "IOException while writing MifareClassic message...", e);
        } finally {
            if (mifare != null) {
                try {
                    mifare.close();
                } catch (IOException e) {
                    cardFound = false;
                    Log.e("test", "Error closing tag...", e);
                }
            }
        }
        return null;
    }

    public void writeTag(Tag tag, String s) {
        mifare = MifareClassic.get(tag);
        try {
            mifare.connect();
            boolean validate=mifare.authenticateSectorWithKeyA(1,c);
            if(validate){
            byte[] b=new byte[16];
                b[0]=(byte)1;
                b[1]=(byte)1;
                b[2]=(byte)1;
                b[3]=(byte)1;
                b[4]=(byte)1;
                b[5]=(byte)1;
                b[6]= (byte)0xFF;
                b[7]=(byte)0x0F;
                b[8]=(byte)0;
                b[9]=(byte)0;
                b[10]=(byte)1;
                b[11]=(byte)1;
                b[12]=(byte)1;
                b[13]=(byte)1;
                b[14]=(byte)1;
                b[15]=(byte)1;

                ByteBuffer bf;
                bf = ByteBuffer.allocate(16);

                quantity = Integer.parseInt(s);
                Log.e("test",quantity+"quantity");
                currentCredit += quantity;
                Log.e("test","currentcredit="+currentCredit);
                bf.putInt(currentCredit);
                mifare.writeBlock(4, bf.array());

            }
            cardFound = true;
            success = true;
            Log.d("test", Arrays.toString(s.getBytes(Charset.forName("US-ASCII"))));

        } catch (IOException e) {
            cardFound = false;
            success = false;
            Log.e("test", "IOException while closing MifareUltralight...", e);
        } finally {
            try {
                mifare.close();
            } catch (IOException e) {

                Log.e("test", "IOException while closing MifareUltralight...", e);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        list = new ArrayList<String>();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ProgressBar loader = (ProgressBar) findViewById(R.id.loader);
        card = (LinearLayout) findViewById(R.id.card);
        credits = (TextView) findViewById(R.id.credits);
        cardActive = (LinearLayout) findViewById(R.id.cardActive);
        cardActive.setVisibility(View.INVISIBLE);
        helperText=(TextView)findViewById(R.id.helperText);
        loader.setVisibility(View.GONE);
        //mTag=(TextView)findViewById(R.id.mTag);
        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        mNfcAdapter.setNdefPushMessageCallback(this, this);
        add = (FloatingActionButton) findViewById(R.id.add);
        myDatabaseAdapter = new MyDatabaseAdapter(this);
        cursor = myDatabaseAdapter.getCursor();
        cursor.getColumnIndex(MySQLiteHelper.getTIMESTAMP());
        simpleCursorAdapter = new SimpleCursorAdapter(this, R.layout.list_layout, cursor, new String[]{MySQLiteHelper.getCARDID(), MySQLiteHelper.getTIMESTAMP(), MySQLiteHelper.getCREDITS()}, new int[]{R.id.cardId, R.id.timestamp, R.id.quantity});
        simpleCursorAdapter.setViewBinder(new SimpleCursorAdapter.ViewBinder() {
            @Override
            public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
                if (columnIndex == 2) {
                    TextView tv = (TextView) view;
                    SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                    Date date = null;
                    try {
                        date = simpleDateFormat1.parse(cursor.getString(2));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    tv.setText(DateFormat.getDateTimeInstance().format(date));
                    return true;
                }
                return false;
            }
        });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //writeTag(tagFromIntent,"");

                if (cardFound) {
                    final Dialog dialog = new Dialog(v.getContext());
                    dialog.setContentView(R.layout.credit_layout);
                    //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    TextView cancelButton = (TextView) dialog.findViewById(R.id.cancelButton);
                    TextView addButton = (TextView) dialog.findViewById(R.id.addButton);
                    cancelButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    addButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            EditText password = (EditText) dialog.findViewById(R.id.password);
                            EditText newCredits = (EditText) dialog.findViewById(R.id.newCredits);
                            Log.d("quench", "data" + password.getText().toString());
                            if (password.getText().toString().equals("quench")) {
                                String nCredits=newCredits.getText().toString();
                                if (newCredits.getText() != null) {
                                    SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("Stats", 0);
                                    if (Integer.parseInt(nCredits)>sharedPreferences.getInt("Credits",0)){
                                            lowBalance();
                                            success=false;
                                    }
                                    else {
                                        writeTag(tagFromIntent, nCredits);
                                    }


                                    if (success) {
                                        //add to db
                                        SharedPreferences.Editor editor = sharedPreferences.edit();
                                        editor.putInt("Credits", sharedPreferences.getInt("Credits",0)-Integer.parseInt(nCredits));
                                        //editor.putInt("Credits", 0);
                                        editor.putInt("Income", sharedPreferences.getInt("Income",0)+Integer.parseInt(nCredits)/10*2);
                                        editor.commit();
                                        cardId=Integer.parseInt(readTag(tagFromIntent,0));
                                        myDatabaseAdapter.insertData(String.valueOf(cardId), quantity);
                                        cursor = myDatabaseAdapter.getCursor();

                                        simpleCursorAdapter.swapCursor(cursor);
                                        //list.add(String.valueOf((int) (Math.random() * 1000)));
                                        simpleCursorAdapter.notifyDataSetChanged();

                                    }
                                }
                            }
                            dialog.dismiss();
                            if (cardFound)
                                credits.setText(readTag(tagFromIntent,1));
                            else {
                                toggleView();
                                showWarning();
                            }
                        }

                    });
                    dialog.show();
                } else {
                    showWarning();
                }
            }
        });


        //simpleCursorAdapter =new SimpleCursorAdapter(this,R.layout.list_layout,cursor,new String[]{"098765","2/1/2016 5:00pm","24"},new int[]{R.id.cardId,R.id.timestamp,R.id.quantity});

        pendingIntent = PendingIntent.getActivity(
                this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
    }
    public void showWarning(){
        AlertDialog warning;
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage(R.string.dialog_message)
                .setTitle(R.string.dialog_title)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        warning = builder.create();
        warning.show();
    }
    public void lowBalance(){
        AlertDialog warning;
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage("You do not have sufficient Credits.Please recharge")
                .setTitle("Insufficeint Credits")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        warning = builder.create();
        warning.show();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_stats) {
            Intent intent=new Intent(MainActivity.this,VendorStats.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.action_call) {
            Intent dial = new Intent(Intent.ACTION_DIAL);
            dial.setData(Uri.parse("tel:"));
            startActivity(dial);
            return true;
        }

        return super.onOptionsItemSelected(item);

    }
    public NdefMessage createNdefMessage(NfcEvent nfcEvent) {
        String message ="100";

        NdefMessage ndefMessage = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            NdefRecord ndefRecord = NdefRecord.createMime("text/plain", message.getBytes());
            ndefMessage = new NdefMessage(ndefRecord);
        }
        return ndefMessage;
    }


}
