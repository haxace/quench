package com.example.harshitsaxena.quench;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by harshitsaxena on 2/29/16.
 */
public class MyDatabaseAdapter {
    String[] columns = {MySQLiteHelper.getUID(), MySQLiteHelper.getCARDID(), MySQLiteHelper.getTIMESTAMP(), MySQLiteHelper.getCREDITS()};
    private SQLiteDatabase sqLiteDatabase;
    private Context context;
    private MySQLiteHelper mySQLiteHelper;

    public MyDatabaseAdapter(Context context) {
        this.context = context;
        mySQLiteHelper = new MySQLiteHelper(context);
        sqLiteDatabase = mySQLiteHelper.getWritableDatabase();

    }

    public long insertData(String cardID, int credits) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(MySQLiteHelper.getCARDID(), cardID);
        //contentValues.put(MySQLiteHelper.getTIMESTAMP(), timestamp);
        contentValues.put(MySQLiteHelper.getCREDITS(), credits);
        long id = sqLiteDatabase.insert(MySQLiteHelper.getTABLE_NAME(), null, contentValues);
        return id;
    }

//    public List<TxnDetails> getData() {
//        List<TxnDetails> txnList = new ArrayList();
//        TxnDetails txnData;
//
//        Cursor c = sqLiteDatabase.query(MySQLiteHelper.getTABLE_NAME(), columns, null, null, null, null, null);
//        StringBuffer s = new StringBuffer();
//        while (c.moveToNext()) {
//
//            int cid = c.getInt(c.getColumnIndex(MySQLiteHelper.getUID()));
//            String name = c.getString(c.getColumnIndex(MySQLiteHelper.getCARDID()));
//            String duration = c.getString(c.getColumnIndex(MySQLiteHelper.getTIMESTAMP()));
//            String progress = c.getString(c.getColumnIndex(MySQLiteHelper.getCREDITS()));
//            txnData = new TxnDetails(cid, name, duration, progress);
//            txnList.add(txnData);
//            s.append("Id= " + cid + " Name= " + name + "duration  " + duration + "  progress" + progress + "\n");
//            Log.d("TestVG", s.toString());
//        }
//
//        return txnList;
//    }

    public Cursor getCursor() {
        //String query="SELECT _id,CardID,Timestamp,Credits FROM MyTable";
        String orderBy = MySQLiteHelper.getTIMESTAMP() + " DESC";
        return sqLiteDatabase.query(MySQLiteHelper.getTABLE_NAME(), columns, null, null, null, null, orderBy);
        //return sqLiteDatabase.rawQuery(query,null);
    }

//    public TxnDetails getData(int id) {
//        TxnDetails exerciseData = new TxnDetails();
//        String selection = MySQLiteHelper.getUID() + " = ? ";
//        String[] args = new String[]{String.valueOf(id)};
//        //Log.d("TestVG", String.valueOf(id) +"id passed");
//        Cursor c = sqLiteDatabase.query(MySQLiteHelper.getTABLE_NAME(), columns, selection, args, null, null, null);
//        Log.d("TestVG", String.valueOf(c.getCount()) +"count");
//        if (c.getCount() != 0) {
//            c.moveToNext();
//        } else {
//            c = sqLiteDatabase.query(MySQLiteHelper.getTABLE_NAME(), columns, null, null, null, null, null);
//            c.moveToFirst();
//
//        }
//        exerciseData.cardId = c.getString(c.getColumnIndex(MySQLiteHelper.getCARDID()));
//        exerciseData.timestamp= c.getString(c.getColumnIndex(MySQLiteHelper.getTIMESTAMP()));
//        exerciseData.credits = c.getInt(c.getColumnIndex(MySQLiteHelper.getCREDITS()));
//        exerciseData.id = c.getInt(c.getColumnIndex(MySQLiteHelper.getUID()));
//        return exerciseData;
//    }


    public void deleteData() {
        sqLiteDatabase.delete(MySQLiteHelper.getTABLE_NAME(), MySQLiteHelper.getUID() + " =?", new String[]{String.valueOf("4")});
    }

//    public void updateData(int id, int progress, int duration) {
//        ContentValues cv = new ContentValues();
//        cv.put(MySQLiteHelper.getPROGRESS(), String.valueOf(progress));
//        cv.put(MySQLiteHelper.getDURATION(), String.valueOf(duration));
//        sqLiteDatabase.update(MySQLiteHelper.getTABLE_NAME(), cv, MySQLiteHelper.getUID() + " =?", new String[]{String.valueOf(id)});
//    }

//    public TxnDetails getPrevious(int id) {
//        TxnDetails exerciseData;
//        Cursor c = getCursor();
//        c.moveToPosition(id);
//        if (!c.moveToPrevious())
//            c.moveToLast();
//        else c.moveToPrevious();
//
//        int cid = c.getInt(c.getColumnIndex(MySQLiteHelper.getUID()));
//        String name = c.getString(c.getColumnIndex(MySQLiteHelper.getNAME()));
//        String duration = c.getString(c.getColumnIndex(MySQLiteHelper.getDURATION()));
//        String progress = c.getString(c.getColumnIndex(MySQLiteHelper.getPROGRESS()));
//        String description = c.getString(c.getColumnIndex(MySQLiteHelper.getNOTES()));
//        exerciseData = new ExerciseData(cid, name, duration, progress, description);
//        return exerciseData;
//    }

//    public TxnDetails getNext(int id) {
//        TxnDetails exerciseData;
//        Cursor c = getCursor();
//        c.moveToPosition(id);
//        Log.d("testa", "1 " + c.getString(c.getColumnIndex(MySQLiteHelper.getUID())));
//        if (!c.moveToNext()) {
//            Log.d("testa", "2 " + c.getString(c.getColumnIndex(MySQLiteHelper.getUID())));
//            c.moveToFirst();
//            Log.d("testa", "3 " + c.getString(c.getColumnIndex(MySQLiteHelper.getUID())));
//        } else {
//            Log.d("testa", "4 " + c.getString(c.getColumnIndex(MySQLiteHelper.getUID())));
//            c.moveToNext();
//            Log.d("testa", "5 " + c.getString(c.getColumnIndex(MySQLiteHelper.getUID())));
//        }
//        int cid = c.getInt(c.getColumnIndex(MySQLiteHelper.getUID()));
//        String name = c.getString(c.getColumnIndex(MySQLiteHelper.getNAME()));
//        String duration = c.getString(c.getColumnIndex(MySQLiteHelper.getDURATION()));
//        String progress = c.getString(c.getColumnIndex(MySQLiteHelper.getPROGRESS()));
//        String description = c.getString(c.getColumnIndex(MySQLiteHelper.getNOTES()));
//        exerciseData = new TxnDetails(cid, name, duration, progress, description);
//        return exerciseData;
//    }

}
