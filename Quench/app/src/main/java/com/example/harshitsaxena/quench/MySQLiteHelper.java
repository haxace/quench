package com.example.harshitsaxena.quench;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.nfc.tech.MifareClassic;
import android.widget.Toast;

/**
 * Created by harshitsaxena on 2/29/16.
 */
public class MySQLiteHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "MyDatabase";
    private static final String TABLE_NAME = "MyTable";
    private static final String UID = "_id";
    private static final String CARDID = "CardID";
    private static final String TIMESTAMP = "Timestamp";
    private static final String CREDITS = "Credits";
    private static final int DATABASE_VERSION = 4;
    private static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    private static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "(" + UID + " INTEGER PRIMARY KEY , " + CARDID + " VARCHAR(255), " + TIMESTAMP + " DATE DEFAULT (datetime('now','localtime')), " + CREDITS + " INTEGER);";
    private Context context;
    private MifareClassic mifareClassic;

    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;

    }

    public static String getUID() {
        return UID;
    }

    public static String getCARDID() {
        return CARDID;
    }

    public static String getTIMESTAMP() {
        return TIMESTAMP;
    }

    public static String getCREDITS() {
        return CREDITS;
    }

    public static String getTABLE_NAME() {
        return TABLE_NAME;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        Toast.makeText(context, "Table created", Toast.LENGTH_LONG).show();
        db.execSQL(CREATE_TABLE);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_TABLE);
        Toast.makeText(context, "OnUpgrade Called", Toast.LENGTH_LONG).show();
        onCreate(db);

    }
}
