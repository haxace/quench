package com.example.harshitsaxena.quench;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;

public class VendorStats extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_stats);
        TextView stock=(TextView)findViewById(R.id.stock);
        TextView income=(TextView)findViewById(R.id.sales);
        TextView recharge=(TextView)findViewById(R.id.recharge);
        SharedPreferences sharedPreferences = getSharedPreferences("Stats", 0);
        stock.setText(String.valueOf(sharedPreferences.getInt("Credits", 0)));
        income.setText(String.valueOf(sharedPreferences.getInt("Income", 0))+" $");
        recharge.setText(String.valueOf(sharedPreferences.getString("Recharge", "12 May, 2016")));
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        LineChart chart = (LineChart) findViewById(R.id.chart);
        chart.setDrawGridBackground(true);
        chart.setDrawBorders(true);
        chart.setDragEnabled(true);
        
        chart.setDescription("Annual Sales");
        chart.setBackgroundColor(new Color().parseColor("#ffffff"));
        ArrayList<String> xVals = new ArrayList<String>();
        xVals.add("Jan"); xVals.add("Feb"); xVals.add("Mar"); xVals.add("Apr");
        xVals.add("May"); xVals.add("Jun"); xVals.add("Jul"); xVals.add("Aug");
        xVals.add("Sep"); xVals.add("Oct"); xVals.add("Nov"); xVals.add("Dec");
        ArrayList<Entry> yVals = new ArrayList<Entry>();
        yVals.add(new Entry(50,0));
        yVals.add(new Entry(30,1));
        yVals.add(new Entry(45,2));
        yVals.add(new Entry(65,3));
        yVals.add(new Entry(50,4));
        yVals.add(new Entry(30,5));
        yVals.add(new Entry(40,6));
        yVals.add(new Entry(30,7));
        yVals.add(new Entry(50,8));
        yVals.add(new Entry(30,9));
        yVals.add(new Entry(45,10));
        yVals.add(new Entry(65,11));
        LineDataSet lineDataSet=new LineDataSet(yVals,"Credits Sold");
        lineDataSet.setAxisDependency(YAxis.AxisDependency.RIGHT);
        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(lineDataSet);
        LineData lineData=new LineData(xVals,dataSets);
        chart.setData(lineData);
    }

}
