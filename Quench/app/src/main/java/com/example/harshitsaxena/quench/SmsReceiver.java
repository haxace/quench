package com.example.harshitsaxena.quench;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.telephony.SmsMessage;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by harshitsaxena on 3/28/16.
 */
public class SmsReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("test","Sms received");
            //detect if sms from supplier
        if(intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")){
            Bundle bundle=intent.getExtras();
            SmsMessage[] msgs = null;
            if(bundle!=null){
                Object[] pdus = (Object[]) bundle.get("pdus");
                msgs = new SmsMessage[pdus.length];
                for (int i=0; i<msgs.length; i++){
                    msgs[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                    String phonenumber=msgs[i].getOriginatingAddress();
                    Log.e("test",phonenumber);
                    if(phonenumber.equals("+13479786575")){

                        String text=msgs[i].getMessageBody().toString();
                        if(text.toLowerCase().charAt(0)=='q') {
                            Log.e("test", text);
                            Integer credit = Integer.parseInt(text.substring(7));
                            Log.e("test", credit + "");
                            SharedPreferences sharedPreferences = context.getSharedPreferences("Stats", 0);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putInt("Credits", sharedPreferences.getInt("Credits", 0) + credit);

                            editor.putString("Recharge", now());
                            editor.commit();
                        }
                    }
                }
            }

        }

    }
    public static String now() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("MMMM d, yyyy");
        return sdf.format(cal.getTime());
    }
}
