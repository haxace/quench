package com.example.harshitsaxena.quench;

import android.app.Activity;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;

public class Agent extends AppCompatActivity  {
    String cr;
    EditText et;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agent);

        NfcAdapter nfc = NfcAdapter.getDefaultAdapter(this);
       et=(EditText)findViewById(R.id.editText);

        if (nfc != null) {
           // Log.w(TAG, "NFC available. Setting Beam Push URI callback");
            //nfc.setNdefPushMessageCallback();
        }
    }
public class Callback implements NfcAdapter.CreateNdefMessageCallback{
    @Override

    public NdefMessage createNdefMessage(NfcEvent nfcEvent) {
        String message = et.getText().toString();

        NdefMessage ndefMessage = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            NdefRecord ndefRecord = NdefRecord.createMime("text/plain", message.getBytes());
            ndefMessage = new NdefMessage(ndefRecord);
        }
        return ndefMessage;
    }
}


}
